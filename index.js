const express = require('express')
const sharp = require('sharp')
const app = express()
const port = 3000

const fs = require('fs');
const path = require('path');

function getRandomImagePath(directory) {
  // Read all files in the specified directory
  const files = fs.readdirSync(directory);

  // Filter out directories from the list of files
  const imageFiles = files.filter(file => {
    const filePath = path.join(directory, file);
    return fs.statSync(filePath).isFile();
  });

  // Generate a random index within the range of available image files
  const randomIndex = Math.floor(Math.random() * imageFiles.length);

  // Get the path of the randomly selected image file
  const randomImagePath = path.join(directory, imageFiles[randomIndex]);

  return randomImagePath;
}

app.get('/:width/:height.jpg', (req, res) => {
  const width = parseInt(req.params.width);
  const height = parseInt(req.params.height);
    const randomImagePath = getRandomImagePath('./images');
  // Load the image using sharp
  sharp(randomImagePath)
    .resize(width, height, { fit: 'cover' }) // Resize and crop the image to the specified dimensions
    .toBuffer()
    .then((outputBuffer) => {
      // Set the appropriate headers for the image response
      res.set('Content-Type', 'image/jpeg');
      res.set('Content-Length', outputBuffer.length);
      res.send(outputBuffer);
    })
    .catch((error) => {
      console.error('Image processing error:', error);
      res.sendStatus(500);
    });
});
app.use(express.static('public'))

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
